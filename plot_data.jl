using Plots, CSV, DataFrames, DataStructures

function main()
    # Question acubesatjoin

    acubesatjoin_answers = ["Yes", "No"]
    acubesatjoin_title = "Did you join AcubeSAT during the COVID-19 pandemic?"
    acubesatjoin_responses = zeros(Int64, length(acubesatjoin_answers))
    name = "acubesatjoin"
    responses_counter = counter(df[!,name])
    acubesatjoin_responses = zeros(Int64, length(acubesatjoin_answers))
    # Output percentages
    for j in 1:length(acubesatjoin_answers)
        acubesatjoin_responses[j] = responses_counter[acubesatjoin_answers[j]]
        percent = 100*responses_counter[acubesatjoin_answers[j]]/length(df[!,name])
        println("For "*acubesatjoin_title*", "*string(round(percent, digits=2))*"% said "*acubesatjoin_answers[j])
    end
    if plot_data
        plot1 = bar(acubesatjoin_answers, acubesatjoin_responses, title = acubesatjoin_title, ylabel = "Number of responses", legend = false, color=:"Coral")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question location

    location_answers = ["Yes", "No"]
    location_title = "As a student/AcubeSAT member, do you live\n in Thessaloniki or in close proximity to Thessaloniki?"
    location_responses = zeros(Int64, length(location_answers))
    name = "location"
    responses_counter = counter(df[!,name])
    location_responses = zeros(Int64, length(location_answers))
    # Output percentages
    for j in 1:length(location_answers)
        location_responses[j] = responses_counter[location_answers[j]]
        percent = 100*responses_counter[location_answers[j]]/length(df[!,name])
        println("For "*location_title*", "*string(round(percent, digits=2))*"% said "*location_answers[j])
    end
    if plot_data
        plot1 = bar(location_answers, location_responses, title = location_title, ylabel = "Number of responses", legend = false, color=:"Cadet Blue")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question problemsrate

    problemsrate_answers = ["Severely Worse", "Worse", "No change", "Improved", "Severely Improved"]
    problemsrate_title = "How much has the following improved or gotten worse\n due to the remote/hybrid work scheme that the team\n has been implementing."
    problemsrate_subquestions = ["Productivity", "Cooperation between team members", "Amount of errors/mistakes", "Psychological well-being", "Fun"]
    problemsrate_responses = zeros(Int64, length(problemsrate_answers))
    for i in 1:length(problemsrate_subquestions)
        name = "problemsrate[SQ00"*string(i)*"]"
        responses_counter = counter(df[!,name])
        # Output percentages
        for j in 1:length(problemsrate_answers)
            problemsrate_responses[j] = responses_counter[problemsrate_answers[j]]
            percent = 100*responses_counter[problemsrate_answers[j]]/length(df[!,name])
            println("For "*problemsrate_subquestions[i]*", "*string(round(percent, digits=2))*"% said "*problemsrate_answers[j])
        end
        if plot_data
            plot1 = bar(problemsrate_answers, problemsrate_responses, title = problemsrate_title, label = problemsrate_subquestions[i], ylabel = "Number of responses", color=:"Dark Slate Gray")
            savefig(plot1, ".\\Figures\\"*name*".png")
        end
    end
    print("\n")

    # Question newmemberintro

    newmemberintro_answers = ["Agree", "Neutral", "Disagree"]
    newmemberintro_title = "New members\n"
    newmemberintro_subquestions = ["I found it easy to ask questions during my training.", "There were no technical issues during the remote introduction.", "At the time, I felt welcome inside the    team.", "I got to meet my fellow subsystem members.", "I got to meet my fellow team members.", "I now feel part of the team.", "I feel welcome to join the team's facilities."]
    newmemberintro_responses = zeros(Int64, length(newmemberintro_answers))
    for i in 1:length(newmemberintro_subquestions)
        name = "newmemberintro[SQ00"*string(i)*"]"
        responses_counter = counter(df[!,name])
        old_members = count(ismissing, df[!,name])
        # Compute responses and output percentages
        for j in 1:length(newmemberintro_answers)
            newmemberintro_responses[j] = responses_counter[newmemberintro_answers[j]]
            percent = 100*responses_counter[newmemberintro_answers[j]]/(length(df[!,name])-old_members)
            println("For "*newmemberintro_subquestions[i]*", "*string(round(percent, digits=2))*"% said "*newmemberintro_answers[j])
        end
        if plot_data
            plot1 = bar(newmemberintro_answers, newmemberintro_responses, title = newmemberintro_title, label = newmemberintro_subquestions[i], ylabel = "Number of responses", color=:"Royal Blue")
            savefig(plot1, ".\\Figures\\"*name*".png")
        end
    end

    # Question ivrwork

    ivrwork_answers = ["Totally Remote", "Hybrid & Mostly Remote", "Hybrid & Mostly On-site", "Totally On-site"]
    ivrwork_answers_labels = ["Totally\nRemote", "Hybrid &\nMostly Remote", "Hybrid &\nMostly On-site", "Totally\nOn-site"]
    ivrwork_title = "How would you prefer the following team activities\n to be done moving forward?"
    ivrwork_subquestions = ["Subsystem work", "Subsystem meetings", "AcubeSAT meetings", "Technical sessions", "Fun activities", "1-on-1 meetings"]
    ivrwork_responses = zeros(Int64, length(ivrwork_answers))
    for i in 1:length(ivrwork_subquestions)
        name = "ivrwork[SQ00"*string(i)*"]"
        responses_counter = counter(df[!,name])
        # Output percentages
        for j in 1:length(ivrwork_answers)
            ivrwork_responses[j] = responses_counter[ivrwork_answers[j]]
            percent = 100*responses_counter[ivrwork_answers[j]]/length(df[!,name])
            println("For "*ivrwork_subquestions[i]*", "*string(round(percent, digits=2))*"% said "*ivrwork_answers[j])
        end
        if plot_data
            plot1 = bar(ivrwork_answers_labels, ivrwork_responses, title = ivrwork_title, label = ivrwork_subquestions[i], ylabel = "Number of responses", color =:"Indian Red")
            savefig(plot1, ".\\Figures\\"*name*".png")
        end
    end
    print("\n")

    # Question timetravelling

    timetravelling_answers = ["0-15 minutes", "15-30 minutes", "30-45 minutes", "45+ minutes"]
    timetravelling_title = "How much time does it take for you\n to reach lab 2 from your home?"
    timetravelling_responses = zeros(Int64, length(timetravelling_answers))
    name = "timetravelling"
    responses_counter = counter(df[!,name])
    old_members = count(ismissing, df[!,name])
    # Compute responses and output percentages
    for j in 1:length(timetravelling_answers)
        timetravelling_responses[j] = responses_counter[timetravelling_answers[j]]
        percent = 100*responses_counter[timetravelling_answers[j]]/(length(df[!,name])-old_members)
        println("For "*timetravelling_title*", "*string(round(percent, digits=2))*"% said "*timetravelling_answers[j])
    end
    if plot_data
        plot1 = bar(timetravelling_answers, timetravelling_responses, title = timetravelling_title, ylabel = "Number of responses", legend = false, color =:"Rebecca Purple")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question distancelab

    distancelab_answers = ["0-0.5 km", "0.5-1 km", "1-2 km", "2-3 km", "3-4 km", "4-5 km", "5+ km", "I live in a different city"]
    distancelab_answers_labels = ["0-0.5 km", "0.5-1 km", "1-2 km", "2-3 km", "3-4 km", "4-5 km", "5+ km"]
    distancelab_title = "How many kilometers away from lab do you live?"
    distancelab_responses = zeros(Int64, length(distancelab_answers)-1)
    name = "distancelab"
    responses_counter = counter(df[!,name])
    old_members = count(ismissing, df[!,name])
    # Compute responses and output percentages
    for j in 1:length(distancelab_answers)-1
        distancelab_responses[j] = responses_counter[distancelab_answers[j]]
        percent = 100*responses_counter[distancelab_answers[j]]/(length(df[!,name])-old_members-responses_counter["I live in a different city"])
        println("For "*distancelab_title*", "*string(round(percent, digits=2))*"% said "*distancelab_answers[j])
    end
    if plot_data
        plot1 = bar(distancelab_answers_labels, distancelab_responses, title = distancelab_title, ylabel = "Number of responses", legend = false, color=:"Indigo")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question timeacubesat

    timeacubesat_title = "What percentage of your daily time\n (ignoring bed time) do you spend for AcubeSAT?"
    timeacubesat_answers = ["0-20%", "20-40%", "40-60%", "60-80%", "80-100%"]
    name = "timeacubesat"
    timeacubesat_responses = collect(skipmissing(df[!,name]))
    number_of_bins = 5
    bin_responses = zeros(Int64, number_of_bins)
    bin_start = 0
    bin_width = 20
    # Compute responses and output percentages
    for i in 1:number_of_bins 
        bin_end = bin_start + bin_width
        if i == number_of_bins 
            bin_responses[i] = count(j->(bin_start<=j<=bin_end), timeacubesat_responses)
        else
            bin_responses[i] = count(j->(bin_start<=j<bin_end), timeacubesat_responses)
        end
        percent = 100*bin_responses[i]/(length(timeacubesat_responses))
        println("For "*timeacubesat_title *", "*string(round(percent, digits=2))*"% said between "*string(bin_start)*"% and "*string(bin_end)*"%.")
        bin_start = bin_end 
    end
    if plot_data
        plot1 = bar(timeacubesat_answers, bin_responses, title = timeacubesat_title, ylabel = "Number of responses", legend = false, color =:"Forest Green")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question timelab

    timelab_title = "What % of your AcubeSAT time\n do you spend in the lab?"
    timelab_answers = ["0-20%", "20-40%", "40-60%", "60-80%", "80-100%"]
    name = "timelab"
    timelab_responses = collect(skipmissing(df[!,name]))
    number_of_bins = 5
    bin_responses = zeros(Int64, number_of_bins)
    bin_start = 0
    bin_width = 20
    # Compute responses and output percentages
    for i in 1:number_of_bins 
        bin_end = bin_start + bin_width
        if i == number_of_bins 
            bin_responses[i] = count(j->(bin_start<=j<=bin_end), timelab_responses)
        else
            bin_responses[i] = count(j->(bin_start<=j<bin_end), timelab_responses)
        end
        percent = 100*bin_responses[i]/(length(timelab_responses))
        println("For "*timelab_title *", "*string(round(percent, digits=2))*"% said between "*string(bin_start)*"% and "*string(bin_end)*"%.")
        bin_start = bin_end 
    end
    if plot_data
        plot1 = bar(timelab_answers, bin_responses, title = timelab_title, ylabel = "Number of responses", legend = false, color=:"Light Salmon")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question timemeetings

    timemeetings_title = "What % of your AcubeSAT time\n do you spend in internal meetings?"
    timemeetings_answers = ["0-20%", "20-40%", "40-60%", "60-80%", "80-100%"]
    name = "timemeetings"
    timemeetings_responses = collect(skipmissing(df[!,name]))
    number_of_bins = 5
    bin_responses = zeros(Int64, number_of_bins)
    bin_start = 0
    bin_width = 20
    # Compute responses and output percentages
    for i in 1:number_of_bins 
        bin_end = bin_start + bin_width
        if i == number_of_bins 
            bin_responses[i] = count(j->(bin_start<=j<=bin_end), timemeetings_responses)
        else
            bin_responses[i] = count(j->(bin_start<=j<bin_end), timemeetings_responses)
        end
        percent = 100*bin_responses[i]/(length(timemeetings_responses))
        println("For "*timemeetings_title *", "*string(round(percent, digits=2))*"% said between "*string(bin_start)*"% and "*string(bin_end)*"%.")
        bin_start = bin_end 
    end
    if plot_data
        plot1 = bar(timemeetings_answers, bin_responses, title = timemeetings_title, ylabel = "Number of responses", legend = false, color=:"Medium Sea Green")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
    print("\n")

    # Question timeoutside

    timeoutside_title = "What % of your AcubeSAT time do you spend\n on the road using your mobile phone?"
    timeoutside_answers = ["0-20%", "20-40%", "40-60%", "60-80%", "80-100%"]
    name = "timeoutside"
    timeoutside_responses = collect(skipmissing(df[!,name]))
    number_of_bins = 5
    bin_responses = zeros(Int64, number_of_bins)
    bin_start = 0
    bin_width = 20
    # Compute responses and output percentages
    for i in 1:number_of_bins 
        bin_end = bin_start + bin_width
        if i == number_of_bins 
            bin_responses[i] = count(j->(bin_start<=j<=bin_end), timeoutside_responses)
        else
            bin_responses[i] = count(j->(bin_start<=j<bin_end), timeoutside_responses)
        end
        percent = 100*bin_responses[i]/(length(timeoutside_responses))
        println("For "*timeoutside_title *", "*string(round(percent, digits=2))*"% said between "*string(bin_start)*"% and "*string(bin_end)*"%.")
        bin_start = bin_end 
    end
    if plot_data
        plot1 = bar(timeoutside_answers, bin_responses, title = timeoutside_title, ylabel = "Number of responses", legend = false, color=:"Slate Blue")
        savefig(plot1, ".\\Figures\\"*name*".png")
    end
end


# Read file
file_name = "survey_results.csv"
df = DataFrame(CSV.File(file_name))

pyplot()
plot_data = true
main()
