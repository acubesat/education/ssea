using Plots, CSV, DataFrames, DataStructures, StatsPlots

function main()
    name1 = "newmemberintro[SQ004]"
    name2 = "newmemberintro[SQ005]"
    newmemberintro_answers = ["Agree", "Neutral", "Disagree"]
    Nx = length(newmemberintro_answers)
    str = fill("",length(newmemberintro_answers))
    for (i,xi) in enumerate(newmemberintro_answers)
        j = findall(x->x==xi, newmemberintro_answers)
        si = " "^(Nx-i)
        @. str[j] = si * string(newmemberintro_answers[j]) * si
    end
    x_values = repeat(str, 2)
    newmemberintro_responses = zeros(Int64, length(newmemberintro_answers))
    newmemberintro_responses2 = zeros(Int64, length(newmemberintro_answers))
    responses_counter1 = counter(df[!,name1])
    responses_counter2 = counter(df[!,name2])
    for j in 1:length(newmemberintro_answers)
        newmemberintro_responses[j] = responses_counter1[newmemberintro_answers[j]]
        newmemberintro_responses2[j] = responses_counter2[newmemberintro_answers[j]]
    end
    y_values = vcat(newmemberintro_responses, newmemberintro_responses2)
    ctg = repeat(["I got to meet my fellow subsystem members.", "I got to meet my fellow team members."], inner = 3)
    grouped_plot = groupedbar(x_values, y_values, group = ctg, ylabel = "Number of responses", title = "New members", color = [:"Royal Blue" :"Indian Red"])
    display(grouped_plot)
    savefig(grouped_plot, ".\\Figures\\combined_image.svg")
    savefig(grouped_plot, ".\\Figures\\combined_image.png")
end


# Read file
file_name = "survey_results.csv"
df = DataFrame(CSV.File(file_name))

pyplot()
plot_data = true
main()
