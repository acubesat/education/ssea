SELECT
    DATE_FORMAT(
        FROM_UNIXTIME(
            `mattermost`.`Posts`.`CreateAt` / 1000
        ),
        '%Y-%m-%d'
    ) AS `date`,
    COUNT(0) AS `count`
FROM
    (
        (
            `mattermost`.`Posts`
        JOIN `mattermost`.`Channels` ON
            (
                `mattermost`.`Posts`.`ChannelId` = `mattermost`.`Channels`.`Id`
            )
        )
    JOIN `mattermost`.`Teams` ON
        (
            `mattermost`.`Channels`.`TeamId` = `mattermost`.`Teams`.`Id`
        )
    )
WHERE
    `mattermost`.`Teams`.`Name` = 'acubesat' AND `mattermost`.`Channels`.`Type` = 'O' AND `mattermost`.`Channels`.`DisplayName` <> 'Random Spam' AND Posts.DeleteAt = 0 AND Posts.Props = '{}'
GROUP BY
    DATE_FORMAT(
        FROM_UNIXTIME(
            `mattermost`.`Posts`.`CreateAt` / 1000
        ),
        '%Y-%m-%d'
    )
ORDER BY
    DATE_FORMAT(
        FROM_UNIXTIME(
            `mattermost`.`Posts`.`CreateAt` / 1000
        ),
        '%Y-%m-%d'
    )
DESC